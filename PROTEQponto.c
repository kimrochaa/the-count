#include <stdio.h>
#include <string.h>
#include <stdlib.h>
typedef struct estrutura1
{
    char nome[100];
    char diadetrabalho[4][8];
    int datadetrabalho[4];
    int horadetrabalhoentrada[4];
    int horadetrabalhosaida[4];
    int horasobg;
    int qthorario;
    int horario[1000][6];
    int horasextras;
    int horasmax;
    int faltas[100];
    double faltasporcentagem[100];
    int faltasatraso[100];
    double faltasatrasoporcentagem[100];
    int qtfaltas;
    int qtfaltasatraso;
    double porc1;
    double porc2;
    double porc3;
    double porc4;
} dados;
void PreenchimentoCalendario (FILE* entrada, int **calendario, int numerodedias);
void CadastramentoFeriado (FILE* entrada, int **calendario);
void CadastroDeMembros (FILE* entrada, int nmembros, dados **mem);
void PreencherNumerosData (int nmembros, dados** mem);
void Calculo (int **calendario, int nmembros, dados** mem);
void ContagemDeHorasMax (int nmembros, int **calendario, dados **mem);
void Estatistica (int nmembros, dados **mem);
void X9 (int nmembros, int **calendario, dados **mem);
void Plot(int nmembros, dados **Membros, FILE* saida, char *nomedomes);
int main (void)
{
    int **calendario, i, j, numerodedias, nmembros, nomedoarquivodesaida[25];
    char nomedomes[30];
    FILE *entrada;
    FILE *saida;
    dados* Membros;
    printf("Todos os direitos reservados.");
    printf("\nDesenvolvido por Kim Rocha.");
    printf("\nContato: kimrochaa@hotmail.com\n");
    printf("Esse software calcula as horas obrigatorias, horas extras, horas planejadas e a porcentagem de presenca do membro.\nPara mais informacoes e instrucoes, leia o readme.");
    printf("\nO programa sera executado e todos os dados estarao em um arquivo chamado 'saida.txt' na pasta onde se encontra esse executavel.");
    printf("\nPara iniciar o programa, tecle enter.\n");
    scanf("%[^\n]");
    entrada=fopen("Dados de Entrada.txt","rt");
    calendario=(int**) calloc(6,sizeof(int*));
    fscanf(entrada, "%*s");
    fscanf(entrada, "\n%[^\n]", nomedomes);
    saida=fopen("Relatorio Mensal.txt","wt");
    fscanf(entrada, "%*s");
    fscanf(entrada, "%d", &numerodedias);
    PreenchimentoCalendario(entrada, calendario, numerodedias);
    CadastramentoFeriado(entrada, calendario);
    fscanf(entrada, "%*s");
    fscanf(entrada,"%d",&nmembros);
    Membros=(dados*) malloc(nmembros*sizeof(dados));
    CadastroDeMembros (entrada, nmembros, &Membros);
    PreencherNumerosData (nmembros, &Membros);
    Calculo(calendario, nmembros, &Membros);
    ContagemDeHorasMax(nmembros, calendario, &Membros);
    Estatistica(nmembros, &Membros);
    X9(nmembros, calendario, &Membros);
    Plot(nmembros, &Membros, saida, nomedomes);
    fclose(entrada);
    fclose(saida);
    return 0;
}
void PreenchimentoCalendario (FILE* entrada, int **calendario, int numerodedias)
{
    int i, inicial, linha=0, coluna=0;
    char diadecomeco[8];
    for (i=0; i<=5; i++)
    {
        calendario[i]=(int*) calloc(7,sizeof(int));
    }
    fscanf(entrada, "%*s");
    fscanf(entrada,"\n%[^\n]", diadecomeco);
    if (strcmp(diadecomeco,"Segunda")==0)
    {
        inicial=0;
    }
    else if (strcmp(diadecomeco,"Terca")==0)
    {
        inicial=1;
    }
    else if (strcmp(diadecomeco,"Quarta")==0)
    {
        inicial=2;
    }
    else if (strcmp(diadecomeco,"Quinta")==0)
    {
        inicial=3;
    }
    else if (strcmp(diadecomeco,"Sexta")==0)
    {
        inicial=4;
    }
    else if (strcmp(diadecomeco,"Sabado")==0)
    {
        inicial=5;
    }
    else if (strcmp(diadecomeco,"Domingo")==0)
    {
        inicial=6;
    }
    for (i=1; i<=numerodedias; i++)
    {
        calendario[linha][coluna+inicial]=i;
        coluna++;
        if (coluna+inicial==7)
        {
            linha++;
            coluna=0;
            inicial=0;
        }
    }
    for (i=0; i<=5; i++)
    {
        calendario[i][5]=0;
        calendario[i][6]=0;
    }
}
void CadastramentoFeriado (FILE* entrada, int **calendario)
{
    int diadoferiado, qtferiados, flag=0, i, j, k;
    fscanf(entrada, "%*s");
    fscanf(entrada,"%d",&qtferiados);
    if (qtferiados>0)
    {
        fscanf(entrada, "%*s");
    for (k=0; k<qtferiados; k++)
    {
        fscanf(entrada, "%d",&diadoferiado);
        for (i=0; i<=5; i++)
        {
            for (j=0; j<=6; j++)
            {
                if (calendario[i][j]==diadoferiado)
                {
                    calendario[i][j]=0;
                }
            }
        }
    }
    }
}
void CadastroDeMembros (FILE* entrada, int nmembros, dados** mem)
{
    int flag=0, cont=0;
    int i, a, b, c, d;
    fscanf(entrada, "%*s");
    for (i=0; i<nmembros; i++)
    {
        flag=0;
        cont=0;
        fscanf(entrada, "\n%[^\n]", &(*mem)[i].nome);
        fscanf(entrada, "%s %2d%*c%2d %2d%*c%2d", &(*mem)[i].diadetrabalho[1], &a, &b, &c, &d);
        (*mem)[i].horadetrabalhoentrada[1]=a*60+b;
        (*mem)[i].horadetrabalhosaida[1]=c*60+d;
        fscanf(entrada, "%s %2d%*c%2d %2d%*c%2d", &(*mem)[i].diadetrabalho[2], &a, &b, &c, &d);
        (*mem)[i].horadetrabalhoentrada[2]=a*60+b;
        (*mem)[i].horadetrabalhosaida[2]=c*60+d;
        fscanf(entrada, "%s %2d%*c%2d %2d%*c%2d", &(*mem)[i].diadetrabalho[3], &a, &b, &c, &d);
        (*mem)[i].horadetrabalhoentrada[3]=a*60+b;
        (*mem)[i].horadetrabalhosaida[3]=c*60+d;
        while (flag==0)
        {
            cont++;
            fscanf(entrada, "%2d%*c%*2d %2d%*c%2d%*c%*2d %2d%*c%2d%*c%*2d", &(*mem)[i].horario[cont][1], &a, &b, &c, &d);
            if ((*mem)[i].horario[cont][1]==0)
            {
                flag=1;
                break;
            }
            (*mem)[i].horario[cont][2]=a*60+b;
            (*mem)[i].horario[cont][3]=c*60+d;
        }
        (*mem)[i].qthorario=cont-1;
        (*mem)[i].horasobg=0;
        (*mem)[i].horasextras=0;
        (*mem)[i].horasmax=0;
        (*mem)[i].qtfaltas=0;
    }
}
void PreencherNumerosData (int nmembros, dados** mem)
{
    int i, j;
    for (i=0; i<nmembros; i++)
    {
        for (j=1; j<=3; j++)
        {
            if (strcmp((*mem)[i].diadetrabalho[j],"Segunda")==0)
            {
                (*mem)[i].datadetrabalho[j]=0;
            }
            else if (strcmp((*mem)[i].diadetrabalho[j],"Terca")==0)
            {
                (*mem)[i].datadetrabalho[j]=1;
            }
            else if (strcmp((*mem)[i].diadetrabalho[j],"Quarta")==0)
            {
                (*mem)[i].datadetrabalho[j]=2;
            }
            else if (strcmp((*mem)[i].diadetrabalho[j],"Quinta")==0)
            {
                (*mem)[i].datadetrabalho[j]=3;
            }
            else if (strcmp((*mem)[i].diadetrabalho[j],"Sexta")==0)
            {
                (*mem)[i].datadetrabalho[j]=4;
            }
        }
    }
}
void Calculo (int **calendario, int nmembros, dados** mem)
{
    int i, j, k, l, m, aux1, aux2;
    for (i=0; i<nmembros; i++)
    {
        for (j=1; j<=(*mem)[i].qthorario; j++)
        {
            aux1=(*mem)[i].horasobg;
            aux2=(*mem)[i].horasextras;
            for (k=0; k<=5; k++)
            {
                for (l=0; l<=7; l++)
                {
                    if (calendario[k][l]==(*mem)[i].horario[j][1])
                    {
                        if (l==(*mem)[i].datadetrabalho[1] && l==(*mem)[i].datadetrabalho[2] && l==(*mem)[i].datadetrabalho[3])
                        {
                            for (m=0; m<(*mem)[i].horario[j][3]-(*mem)[i].horario[j][2]; m++)
                            {
                                if ((*mem)[i].horario[j][2]+m>=(*mem)[i].horadetrabalhoentrada[1] && (*mem)[i].horario[j][2]+m<(*mem)[i].horadetrabalhosaida[1])
                                {
                                    (*mem)[i].horasobg++;
                                }
                                else if ((*mem)[i].horario[j][2]+m>=(*mem)[i].horadetrabalhoentrada[2] && (*mem)[i].horario[j][2]+m<(*mem)[i].horadetrabalhosaida[2])
                                {
                                    (*mem)[i].horasobg++;
                                }
                                else if ((*mem)[i].horario[j][2]+m>=(*mem)[i].horadetrabalhoentrada[3] && (*mem)[i].horario[j][2]+m<(*mem)[i].horadetrabalhosaida[3])
                                {
                                    (*mem)[i].horasobg++;
                                }
                                else
                                {
                                    (*mem)[i].horasextras++;
                                }
                            }
                        }
                        else if (l==(*mem)[i].datadetrabalho[1] && l==(*mem)[i].datadetrabalho[2])
                        {
                            for (m=0; m<(*mem)[i].horario[j][3]-(*mem)[i].horario[j][2]; m++)
                            {
                                if ((*mem)[i].horario[j][2]+m>=(*mem)[i].horadetrabalhoentrada[1] && (*mem)[i].horario[j][2]+m<(*mem)[i].horadetrabalhosaida[1])
                                {
                                    (*mem)[i].horasobg++;
                                }
                                else if ((*mem)[i].horario[j][2]+m>=(*mem)[i].horadetrabalhoentrada[2] && (*mem)[i].horario[j][2]+m<(*mem)[i].horadetrabalhosaida[2])
                                {
                                    (*mem)[i].horasobg++;
                                }
                                else
                                {
                                    (*mem)[i].horasextras++;
                                }
                            }
                        }
                        else if (l==(*mem)[i].datadetrabalho[2] && l==(*mem)[i].datadetrabalho[3])
                        {
                            for (m=0; m<(*mem)[i].horario[j][3]-(*mem)[i].horario[j][2]; m++)
                            {
                                if ((*mem)[i].horario[j][2]+m>=(*mem)[i].horadetrabalhoentrada[2] && (*mem)[i].horario[j][2]+m<(*mem)[i].horadetrabalhosaida[2])
                                {
                                    (*mem)[i].horasobg++;
                                }
                                else if ((*mem)[i].horario[j][2]+m>=(*mem)[i].horadetrabalhoentrada[3] && (*mem)[i].horario[j][2]+m<(*mem)[i].horadetrabalhosaida[3])
                                {
                                    (*mem)[i].horasobg++;
                                }
                                else
                                {
                                    (*mem)[i].horasextras++;
                                }
                            }
                        }
                        else if (l==(*mem)[i].datadetrabalho[1])
                        {
                            for (m=0; m<(*mem)[i].horario[j][3]-(*mem)[i].horario[j][2]; m++)
                            {
                                if ((*mem)[i].horario[j][2]+m>=(*mem)[i].horadetrabalhoentrada[1] && (*mem)[i].horario[j][2]+m<(*mem)[i].horadetrabalhosaida[1])
                                {
                                    (*mem)[i].horasobg++;
                                }
                                else
                                {
                                    (*mem)[i].horasextras++;
                                }
                            }
                        }
                        else if (l==(*mem)[i].datadetrabalho[2])
                        {
                            for (m=0; m<(*mem)[i].horario[j][3]-(*mem)[i].horario[j][2]; m++)
                            {
                                if ((*mem)[i].horario[j][2]+m>=(*mem)[i].horadetrabalhoentrada[2] && (*mem)[i].horario[j][2]+m<(*mem)[i].horadetrabalhosaida[2])
                                {
                                    (*mem)[i].horasobg++;
                                }
                                else
                                {
                                    (*mem)[i].horasextras++;
                                }
                            }
                        }
                        else if (l==(*mem)[i].datadetrabalho[3])
                        {
                            for (m=0; m<(*mem)[i].horario[j][3]-(*mem)[i].horario[j][2]; m++)
                            {
                                if ((*mem)[i].horario[j][2]+m>=(*mem)[i].horadetrabalhoentrada[3] && (*mem)[i].horario[j][2]+m<(*mem)[i].horadetrabalhosaida[3])
                                {
                                    (*mem)[i].horasobg++;
                                }
                                else
                                {
                                    (*mem)[i].horasextras++;
                                }
                            }
                        }
                        else
                        {
                            (*mem)[i].horasextras+=(*mem)[i].horario[j][3]-(*mem)[i].horario[j][2];
                        }
                    }
                }
            }
            (*mem)[i].horario[j][4]=(*mem)[i].horasobg-aux1;
            (*mem)[i].horario[j][5]=(*mem)[i].horasextras-aux2;
        }

    }
}
void ContagemDeHorasMax (int nmembros, int **calendario, dados **mem)
{
    int i, j, k;
    for (i=0; i<nmembros; i++)
    {
        for (j=1; j<=3; j++)
        {
            if (calendario[0][(*mem)[i].datadetrabalho[j]]!=0)
            {
                (*mem)[i].horasmax+=(*mem)[i].horadetrabalhosaida[j]-(*mem)[i].horadetrabalhoentrada[j];
            }
            if (calendario[1][(*mem)[i].datadetrabalho[j]]!=0)
            {
                (*mem)[i].horasmax+=(*mem)[i].horadetrabalhosaida[j]-(*mem)[i].horadetrabalhoentrada[j];
            }
            if (calendario[2][(*mem)[i].datadetrabalho[j]]!=0)
            {
                (*mem)[i].horasmax+=(*mem)[i].horadetrabalhosaida[j]-(*mem)[i].horadetrabalhoentrada[j];
            }
            if (calendario[3][(*mem)[i].datadetrabalho[j]]!=0)
            {
                (*mem)[i].horasmax+=(*mem)[i].horadetrabalhosaida[j]-(*mem)[i].horadetrabalhoentrada[j];
            }
            if (calendario[4][(*mem)[i].datadetrabalho[j]]!=0)
            {
                (*mem)[i].horasmax+=(*mem)[i].horadetrabalhosaida[j]-(*mem)[i].horadetrabalhoentrada[j];
            }
            if (calendario[5][(*mem)[i].datadetrabalho[j]]!=0)
            {
                (*mem)[i].horasmax+=(*mem)[i].horadetrabalhosaida[j]-(*mem)[i].horadetrabalhoentrada[j];
            }
        }
    }
}
void Estatistica (int nmembros, dados **mem)
{
    int i;
    double a;
    double b;
    double c;
    double d;
    for (i=0; i<nmembros; i++)
    {
        a=(*mem)[i].horasobg;
        b=(*mem)[i].horasextras;
        c=(*mem)[i].horasmax;
        d=(*mem)[i].horasobg+(*mem)[i].horasextras;
        (*mem)[i].porc1=a/c;
        (*mem)[i].porc2=b/a;
        (*mem)[i].porc3=b/c;
        (*mem)[i].porc4=d/c;

    }
}
void X9 (int nmembros, int **calendario, dados **mem)
{
    int auxf, i, j, l, k, total, parcial, parcial1, parcial2, parcial3, vetor[6], vetor1[6], vetor2[6], vetor3[6], contdias=0, contdias1=0, contdias2=0, contdias3=0, total1, total2, total3, contfaltas, contfaltasatraso;
    double a, b, c;
    for (i=0; i<nmembros; i++)
    {
        total=0;
        contdias=0;
        contdias1=0;
        contdias2=0;
        contdias3=0;
        contfaltas=0;
        contfaltasatraso=0;
        if ((*mem)[i].datadetrabalho[1]==(*mem)[i].datadetrabalho[2] && (*mem)[i].datadetrabalho[2]==(*mem)[i].datadetrabalho[3])
        {
            total=(*mem)[i].horadetrabalhosaida[1]-(*mem)[i].horadetrabalhoentrada[1]+(*mem)[i].horadetrabalhosaida[2]-(*mem)[i].horadetrabalhoentrada[2]+(*mem)[i].horadetrabalhosaida[3]-(*mem)[i].horadetrabalhoentrada[3];
            for (j=0; j<=5; j++)
            {
                for (k=0; k<=7; k++)
                {
                    if (k==(*mem)[i].datadetrabalho[1] && calendario[j][k]!=0)
                    {
                        contdias++;
                        vetor[contdias]=calendario[j][k];
                    }
                }
            }
            for (j=1; j<=contdias; j++)
            {
                parcial=0;
                for (k=1; k<=(*mem)[i].qthorario; k++)
                {
                    if (vetor[j]==(*mem)[i].horario[k][1])
                    {
                        parcial+=(*mem)[i].horario[k][4];
                    }
                }
                b=parcial;
                c=total;
                a=b/c;
                if (a<0.8 && a>=0.05)
                {
                    contfaltasatraso++;
                    (*mem)[i].faltasatraso[contfaltasatraso]=vetor[j];
                    (*mem)[i].faltasatrasoporcentagem[contfaltasatraso]=total-parcial;
                }
                else if (a<0.05)
                {
                    contfaltas++;
                    (*mem)[i].faltas[contfaltas]=vetor[j];
                    (*mem)[i].faltasporcentagem[contfaltas]=total-parcial;
                }
            }
            (*mem)[i].qtfaltasatraso=contfaltasatraso;
            (*mem)[i].qtfaltas=contfaltas;
        }
        else if ((*mem)[i].datadetrabalho[1]==(*mem)[i].datadetrabalho[2])
        {
            total=(*mem)[i].horadetrabalhosaida[1]-(*mem)[i].horadetrabalhoentrada[1]+(*mem)[i].horadetrabalhosaida[2]-(*mem)[i].horadetrabalhoentrada[2];
            total1=(*mem)[i].horadetrabalhosaida[3]-(*mem)[i].horadetrabalhoentrada[3];
            for (j=0; j<=5; j++)
            {
                for (k=0; k<=7; k++)
                {
                    if (k==(*mem)[i].datadetrabalho[1] && calendario[j][k]!=0)
                    {
                        contdias++;
                        vetor[contdias]=calendario[j][k];
                    }
                    else if (k==(*mem)[i].datadetrabalho[3] && calendario[j][k]!=0)
                    {
                        contdias1++;
                        vetor1[contdias1]=calendario[j][k];
                    }
                }
            }
            for (j=1; j<=contdias; j++)
            {
                parcial=0;
                for (k=1; k<=(*mem)[i].qthorario; k++)
                {
                    if (vetor[j]==(*mem)[i].horario[k][1])
                    {
                        parcial+=(*mem)[i].horario[k][4];
                    }
                }
                b=parcial;
                c=total;
                a=b/c;
                if (a<0.8 && a>=0.05)
                {
                    contfaltasatraso++;
                    (*mem)[i].faltasatraso[contfaltasatraso]=vetor[j];
                    (*mem)[i].faltasatrasoporcentagem[contfaltasatraso]=total-parcial;
                }
                else if (a<0.05)
                {
                    contfaltas++;
                    (*mem)[i].faltas[contfaltas]=vetor[j];
                    (*mem)[i].faltasporcentagem[contfaltas]=total-parcial;
                }
            }
            for (j=1; j<=contdias1; j++)
            {
                parcial=0;
                for (k=1; k<=(*mem)[i].qthorario; k++)
                {
                    if (vetor1[j]==(*mem)[i].horario[k][1])
                    {
                        parcial+=(*mem)[i].horario[k][4];
                    }
                }
                b=parcial;
                c=total1;
                a=b/c;
                if (a<0.8 && a>=0.05)
                {
                    contfaltasatraso++;
                    (*mem)[i].faltasatraso[contfaltasatraso]=vetor1[j];
                    (*mem)[i].faltasatrasoporcentagem[contfaltasatraso]=total1-parcial;
                }
                else if (a<0.05)
                {
                    contfaltas++;
                    (*mem)[i].faltas[contfaltas]=vetor1[j];
                    (*mem)[i].faltasatrasoporcentagem[contfaltas]=total1-parcial;
                }
            }
            (*mem)[i].qtfaltasatraso=contfaltasatraso;
            (*mem)[i].qtfaltas=contfaltas;
        }
        else if ((*mem)[i].datadetrabalho[2]==(*mem)[i].datadetrabalho[3])
        {
            total=(*mem)[i].horadetrabalhosaida[2]-(*mem)[i].horadetrabalhoentrada[2]+(*mem)[i].horadetrabalhosaida[3]-(*mem)[i].horadetrabalhoentrada[3];
            total1=(*mem)[i].horadetrabalhosaida[1]-(*mem)[i].horadetrabalhoentrada[1];
            for (j=0; j<=5; j++)
            {
                for (k=0; k<=7; k++)
                {
                    if (k==(*mem)[i].datadetrabalho[2] && calendario[j][k]!=0)
                    {
                        contdias++;
                        vetor[contdias]=calendario[j][k];
                    }
                    else if (k==(*mem)[i].datadetrabalho[1] && calendario[j][k]!=0)
                    {
                        contdias1++;
                        vetor1[contdias1]=calendario[j][k];
                    }
                }
            }
            for (j=1; j<=contdias; j++)
            {
                parcial=0;
                for (k=1; k<=(*mem)[i].qthorario; k++)
                {
                    if (vetor[j]==(*mem)[i].horario[k][1])
                    {
                        parcial+=(*mem)[i].horario[k][4];
                    }
                }
                b=parcial;
                c=total;
                a=b/c;
                if (a<0.8 && a>=0.05)
                {
                    contfaltasatraso++;
                    (*mem)[i].faltasatraso[contfaltasatraso]=vetor[j];
                    (*mem)[i].faltasatrasoporcentagem[contfaltasatraso]=100*a;
                }
                else if (a<0.05)
                {
                    contfaltas++;
                    (*mem)[i].faltas[contfaltas]=vetor[j];
                    (*mem)[i].faltasporcentagem[contfaltas]=100*a;
                }
            }
            for (j=1; j<=contdias1; j++)
            {
                parcial=0;
                for (k=1; k<=(*mem)[i].qthorario; k++)
                {
                    if (vetor1[j]==(*mem)[i].horario[k][1])
                    {
                        parcial+=(*mem)[i].horario[k][4];
                    }
                }
                b=parcial;
                c=total1;
                a=b/c;
                if (a<0.8 && a>=0.05)
                {
                    contfaltasatraso++;
                    (*mem)[i].faltasatraso[contfaltasatraso]=vetor1[j];
                    (*mem)[i].faltasatrasoporcentagem[contfaltasatraso]=total1-parcial;
                }
                else if (a<0.05)
                {
                    contfaltas++;
                    (*mem)[i].faltas[contfaltas]=vetor1[j];
                    (*mem)[i].faltasporcentagem[contfaltas]=total1-parcial;
                }
            }
            (*mem)[i].qtfaltasatraso=contfaltasatraso;
            (*mem)[i].qtfaltas=contfaltas;
        }
        else
        {
            total1=(*mem)[i].horadetrabalhosaida[1]-(*mem)[i].horadetrabalhoentrada[1];
            total2=(*mem)[i].horadetrabalhosaida[2]-(*mem)[i].horadetrabalhoentrada[2];
            total3=(*mem)[i].horadetrabalhosaida[3]-(*mem)[i].horadetrabalhoentrada[3];
            for (j=0; j<=5; j++)
            {
                for (k=0; k<=7; k++)
                {
                    if (k==(*mem)[i].datadetrabalho[1] && calendario[j][k]!=0)
                    {
                        contdias1++;
                        vetor1[contdias1]=calendario[j][k];
                    }
                    else if (k==(*mem)[i].datadetrabalho[2] && calendario[j][k]!=0)
                    {
                        contdias2++;
                        vetor2[contdias2]=calendario[j][k];
                    }
                    else if (k==(*mem)[i].datadetrabalho[3] && calendario[j][k]!=0)
                    {
                        contdias3++;
                        vetor3[contdias3]=calendario[j][k];
                    }
                }
            }
            for (j=1; j<=contdias1; j++)
            {
                parcial=0;
                for (k=1; k<=(*mem)[i].qthorario; k++)
                {
                    if (vetor1[j]==(*mem)[i].horario[k][1])
                    {
                        parcial+=(*mem)[i].horario[k][4];
                    }
                }
                b=parcial;
                c=total1;
                a=b/c;
                if (a<0.8 && a>=0.05)
                {
                    contfaltasatraso++;
                    (*mem)[i].faltasatraso[contfaltasatraso]=vetor1[j];
                    (*mem)[i].faltasatrasoporcentagem[contfaltasatraso]=total1-parcial;
                }
                else if (a<0.05)
                {
                    contfaltas++;
                    (*mem)[i].faltas[contfaltas]=vetor1[j];
                    (*mem)[i].faltasporcentagem[contfaltas]=total1-parcial;
                }
            }
            for (j=1; j<=contdias2; j++)
            {
                parcial=0;
                for (k=1; k<=(*mem)[i].qthorario; k++)
                {
                    if (vetor2[j]==(*mem)[i].horario[k][1])
                    {
                        parcial+=(*mem)[i].horario[k][4];
                    }
                }
                b=parcial;
                c=total2;
                a=b/c;
                if (a<0.8 && a>=0.05)
                {
                    contfaltasatraso++;
                    (*mem)[i].faltasatraso[contfaltasatraso]=vetor2[j];
                    (*mem)[i].faltasatrasoporcentagem[contfaltasatraso]=total2-parcial;
                }
                else if (a<0.05)
                {
                    contfaltas++;
                    (*mem)[i].faltas[contfaltas]=vetor2[j];
                    (*mem)[i].faltasporcentagem[contfaltas]=total2-parcial;
                }
            }
            for (j=1; j<=contdias3; j++)
            {
                parcial=0;
                for (k=1; k<=(*mem)[i].qthorario; k++)
                {
                    if (vetor3[j]==(*mem)[i].horario[k][1])
                    {
                        parcial+=(*mem)[i].horario[k][4];
                    }
                }
                b=parcial;
                c=total3;
                a=b/c;
                if (a<0.8 && a>=0.05)
                {
                    contfaltasatraso++;
                    (*mem)[i].faltasatraso[contfaltasatraso]=vetor3[j];
                    (*mem)[i].faltasatrasoporcentagem[contfaltasatraso]=total3-parcial;
                }
                else if (a<0.05)
                {
                    contfaltas++;
                    (*mem)[i].faltas[contfaltas]=vetor3[j];
                    (*mem)[i].faltasporcentagem[contfaltas]=total3-parcial;
                }
            }
            (*mem)[i].qtfaltas=contfaltas;
            (*mem)[i].qtfaltasatraso=contfaltasatraso;
        }
        for (j=1; j<=(*mem)[i].qtfaltas; j++)
        {
            for (k=1; k<=(*mem)[i].qtfaltas; k++)
            {
                if ((*mem)[i].faltas[j]<(*mem)[i].faltas[k])
                {
                    auxf=(*mem)[i].faltas[j];
                    (*mem)[i].faltas[j]=(*mem)[i].faltas[k];
                    (*mem)[i].faltas[k]=auxf;
                }
            }

        }
        for (j=1; j<=(*mem)[i].qtfaltasatraso; j++)
        {
            for (k=1; k<=(*mem)[i].qtfaltasatraso; k++)
            {
                if ((*mem)[i].faltasatraso[j]<(*mem)[i].faltasatraso[k])
                {
                    auxf=(*mem)[i].faltasatraso[j];
                    (*mem)[i].faltasatraso[j]=(*mem)[i].faltasatraso[k];
                    (*mem)[i].faltasatraso[k]=auxf;
                }
            }

        }
    }
}
void Plot(int nmembros, dados **Membros, FILE* saida, char *nomedomes)
{
    int i, j, menor, maior;
    fprintf(saida, "\t\t\t\t\t\t\t\tRelatorio Mensal de Acompanhamento de Horarios\n\n\n");
    fprintf(saida, "- Relatorio Geral de Acompanhamento de Horarios\n");
    fprintf(saida, "M�S: %s\n\n",nomedomes);
    fprintf(saida, "Membros que trabalharam abaixo de 75%%: \n",nomedomes);
    for (i=0; i<nmembros; i++)
    {
        if ((*Membros)[i].porc1<0.75)
        {
            fprintf(saida, "%s\n", (*Membros)[i].nome);
        }
    }
    fprintf(saida, "\n\n\n");
    fprintf(saida, "- Relatorio Individual de Acompanhamento de Horarios\n");
    for (i=0; i<nmembros; i++)
    {
        fprintf(saida, "Nome do membro: %s\n", (*Membros)[i].nome);
        fprintf(saida, "Tempo de trabalho obrigat�rio: %d minutos (%dh%dmin)\n", (*Membros)[i].horasobg, (*Membros)[i].horasobg/60, (*Membros)[i].horasobg%60);
        fprintf(saida, "Tempo de trabalho extra: %d minutos (%dh%dmin)\n", (*Membros)[i].horasextras, (*Membros)[i].horasextras/60, (*Membros)[i].horasextras%60);
        fprintf(saida, "Tempo de trabalho m�ximo: %d minutos (%dh%dmin)\n", (*Membros)[i].horasmax, (*Membros)[i].horasmax/60, (*Membros)[i].horasmax%60);
        fprintf(saida, "Porcentagem de tempo obrigat�rio: %.2f%%\n", 100*(*Membros)[i].porc1);
        fprintf(saida, "Porcentagem de tempo total: %.2f%%\n", 100*(*Membros)[i].porc4);
        fprintf(saida, "O membro trabalhou %.1f vezes mais em hor�rio extra do que em hor�rio obrigat�rio.\n", (*Membros)[i].porc2);
        fprintf(saida, "Datas em que faltou ou atrasou:\n\n");
        fprintf(saida, "Data  -  Atraso\n");
        for (j=1; j<=(*Membros)[i].qtfaltasatraso; j++)
        {
            fprintf(saida, "-%d-  |  -%.0f min-\n", (*Membros)[i].faltasatraso[j],(*Membros)[i].faltasatrasoporcentagem[j]);
        }
        for (j=1; j<=(*Membros)[i].qtfaltas; j++)
        {
            fprintf(saida, "-%d-  |  N�o foi\n", (*Membros)[i].faltas[j]);
        }
        fprintf(saida, "\n");
        fprintf(saida, "\n\n\n\n");
    }

    fprintf(saida, "-----------Parte para CTRL+C/CTRL+V em Avalia��o Mensal----------------\n");

    fprintf(saida, "Horas trabalhadas obrigat�rias em ordem cadastrada:\n");
    for (i=0; i<nmembros; i++)
    {
        fprintf(saida,"%d\n",(*Membros)[i].horasobg);
    }
    fprintf(saida, "Horas m�ximas obrigat�rias em ordem cadastrada:\n");
    for (i=0; i<nmembros; i++)
    {
        fprintf(saida,"%d\n",(*Membros)[i].horasmax);
    }
    fprintf(saida, "Horas extras em ordem cadastrada:\n");
    for (i=0; i<nmembros; i++)
    {
        fprintf(saida,"%d\n",(*Membros)[i].horasextras);
    }
    fprintf(saida, "\n\n\n\nTodos os direitos reservados.");
    fprintf(saida, "\n\n\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tDesenvolvido por Kim Rocha.");

}
